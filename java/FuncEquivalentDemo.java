package com.demo.functional_interface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Predicate;

public class FuncEquivalentDemo {
	private static Person person1 = new Person("Pierre", 27);
	private static Person person2 = new Person("Pauline", 39);
	private static Person person3 = new Person("Jacques", 41);

	static int sumAges(Person p1, Person p2, Person p3) {
		return p1.getAge() + p2.getAge() + p3.getAge();
	}

	static int averageAges(Person p1, Person p2, Person p3) {
		return (p1.getAge() + p2.getAge() + p3.getAge()) / 3;
	}

	public static void threePeopleDemo() {
		System.out.println(person1);
		System.out.println(person2);
		System.out.println(person3);

		// delegate prenant 3 personnes en param et renvoi un entier
		// SumAges assigné
		// ThreePeople myFunc = SumAges;
		askFunc(FuncEquivalentDemo::sumAges);

		// System.out.println(myFunc(person1, person2, person3));
		// System.out.println(myFunc(person1, person2, person3));

		// AverageAges assigné
		// myFunc = AverageAges;
		askFunc(FuncEquivalentDemo::averageAges);

		// System.out.println(myFunc(person1, person2, person3));

		// Déclaration anonyme en bloc
		askFunc((Person p1, Person p2, Person p3) -> {

			return Collections.min(new ArrayList<Integer>() {

				private static final long serialVersionUID = -1886324149251243947L;

				{
					add(p1.getAge());
					add(p2.getAge());
					add(p3.getAge());
				}
			});
		});

		Person mikoshi = new Person("Mikoshi", 38);
		Person liam = new Person("Liam", 12);
		Person renata = new Person("Renata", 23);

		askFunc(mikoshi, liam, renata, FuncEquivalentDemo::sumAges);
		askFunc(mikoshi, liam, renata, FuncEquivalentDemo::averageAges);

		// Déclaration anonyme avec paramètres pesonnalisables
		// C'est surtout ça que j'ai envie de pouvoir faire

		askFunc(mikoshi, liam, renata, (p1, p2, p3) -> {
			System.out.println(p1.getName() + " names's length is " + p1.getName().length());
			System.out.println(p2.getName() + " names's length is " + p2.getName().length());
			System.out.println(p3.getName() + " names's length is " + p3.getName().length());

			int total = p1.getName().length() + p2.getName().length() + p3.getName().length();
			System.out.println(total);

			return total;
		});
	}

	public static void furtherDemo() {
//	    Predicate<String> isEmpty = String::isEmpty;
//	    System.out.println(isEmpty.test("Hello"));
//	    System.out.println(isEmpty.negate().test("World"));

		ThreePeople tp = FuncEquivalentDemo::sumAges;

		System.out.println(tp.doWithAges(new Person("Jake", 57), new Person("Bill", 25), new Person("Laura", 18)));
		
		tp = FuncEquivalentDemo::averageAges;
		
		System.out.println(tp.doWithAges(new Person("Jake", 57), new Person("Bill", 25), new Person("Laura", 18)));
	}

	static void askFunc(ThreePeople threePeople) {
		System.out.print("simple ");
		askFunc(person1, person2, person3, threePeople);
	}

	static void askFunc(Person person1, Person person2, Person person3, ThreePeople threePeople) {
		System.out.println("askFunc -> " + threePeople.doWithAges(person1, person2, person3));
	}
}
