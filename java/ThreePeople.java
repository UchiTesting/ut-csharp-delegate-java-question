package com.demo.functional_interface;

@FunctionalInterface
public interface ThreePeople {
	int doWithAges(Person p1, Person p2, Person p3);
}
