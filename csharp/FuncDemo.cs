using Pb.FuncDemo;

public class FuncDemo
{
    private static Person person1 = new Person("Pierre", 27);
    private static Person person2 = new Person("Pauline", 39);
    private static Person person3 = new Person("Jacques", 41);

    static int SumAges(Person p1, Person p2, Person p3) => p1.Age + p2.Age + p3.Age;
    static int AverageAges(Person p1, Person p2, Person p3) => (p1.Age + p2.Age + p3.Age) / 3;

    public static void ThreePeopleDemo()
    {
        Console.WriteLine(person1);
        Console.WriteLine(person2);
        Console.WriteLine(person3);

        // delegate prenant 3 personnes en param et renvoi un entier
        // SumAges assigné
        Func<Person, Person, Person, int> myFunc = SumAges;

        System.Console.WriteLine(myFunc(person1, person2, person3));

        // AverageAges assigné
        myFunc = AverageAges;

        System.Console.WriteLine(myFunc.Invoke(person1, person2, person3));

        // Déclaration anonyme en bloc
        AskFunc((Person p1, Person p2, Person p3) =>
        {
            // return new List<Person>{p1,p2,p3}.Min();
            int temp = new List<int> { p1.Age, p2.Age, p3.Age }.Min();

            return temp;
        });

        Person mikoshi = new Person("Mikoshi", 38);
        Person liam = new Person("Liam", 12);
        Person renata = new Person("Renata", 23);

        // Déclaration anonyme avec paramètres pesonnalisables
        // C'est surtout ça que j'ai envie de pouvoir faire
        AskFunc(mikoshi, liam, renata, (p1, p2, p3) =>
        {
            System.Console.WriteLine($"{p1.Name} name's length is {p1.Name.Count()}");
            System.Console.WriteLine($"{p2.Name} name's length is {p2.Name.Count()}");
            System.Console.WriteLine($"{p3.Name} name's length is {p3.Name.Count()}");

            int total = p1.Name.Count() + p2.Name.Count() + p3.Name.Count();
            System.Console.WriteLine($"Total {total}");

            return total;
        });
    }

    static void AskFunc(Func<Person, Person, Person, int> threePeopleFunc)
    {
        AskFunc(person1, person2, person3, threePeopleFunc);
    }

    static void AskFunc(Person person1, Person person2, Person person3, Func<Person, Person, Person, int> threePeopleFunc)
    {
        System.Console.WriteLine($"AskFunc => {threePeopleFunc(person1, person2, person3)}");
    }
}