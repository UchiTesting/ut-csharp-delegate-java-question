Equivalent to C# `delegate` with Java
=====================================
Equivalent au `delegate`  de C# avec Java
=========================================

> Je ne fais le paragraphe d'intro qu'en anglais.

The idea is to try finding a simple way to reproduce the way C# delegates work in Java.

For that matter the example project relies on a C# `Func<>` delegate taking 3 `Person` as parameters
and returning an integer. The code will then use lambdas in both languages to perform different actions.

In C# the `delegate` keyword can be seen as a pointer to a function.

The following code have a few occurences of such statements.

```csharp
// A classic function returning the sum of a trio's age
static int SumAges(Person p1, Person p2, Person p3) => p1.Age + p2.Age + p3.Age;

// A delegate based on the predefined `Func<>`. The last generic parameter is the return type
Func<Person, Person, Person, int> myFunc = SumAges;

// Call to the delegate function
System.Console.WriteLine(myFunc(person1, person2, person3));

// Could also have been
System.Console.WriteLine(myFunc.Invoke(person1, person2, person3));
```

Most of the C# code could be translated to Java. 
I could not find how to reproduce something similar to the above snippet.

Should anyone know any way to do it, I'd love to know. Hopefully Java would add such thing (if it does not exist already).

## Solution

> Thanks to Jason R. - Sr Java Dev @ Google for the help.

The missing bit is to be managed in a different fashion with Java.  
Not as nice as C# delegates. Cannot prepare once and call many.  
Simply call many but still a nice syntax. (I ❤ it.) 

```java
// delegate prenant 3 personnes en param et renvoi un entier
// SumAges assigné
// ThreePeople myFunc = SumAges; // Not possible with Java (yet?)
// Simply use reference where needs be
askFunc(FuncEquivalentDemo::sumAges);
askFunc(FuncEquivalentDemo::averageAges);
// or with params
askFunc(mikoshi, liam, renata, FuncEquivalentDemo::sumAges);
askFunc(mikoshi, liam, renata, FuncEquivalentDemo::averageAges);
```

### Addendum

Actually you can do something close enough.

Now I think my landmarks are set. 😎

```java
public static void furtherDemo() {
	// Predicate<String> isEmpty = String::isEmpty;
	// System.out.println(isEmpty.test("Hello"));
	// System.out.println(isEmpty.negate().test("World"));

	ThreePeople tp = FuncEquivalentDemo::sumAges;
	System.out.println(tp
		.doWithAges(new Person("Jake", 57), 
			new Person("Bill", 25), 
			new Person("Laura", 18)));
	
	tp = FuncEquivalentDemo::averageAges;		
	System.out.println(tp
		.doWithAges(new Person("Jake", 57), 
			new Person("Bill", 25), 
			new Person("Laura", 18)));
}
```

## C#

> Class(e) `Person`

```csharp
class Person
{
    public string Name { get; set; }
    public int Age { get; set; }

    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public override string ToString()
    {
        return $"[Name: {Name}, Age: {Age}]";
    }
}
```
> Misc. examples. `ThreePeopleDemo()` is ran from `Main()`
> Divers exemples. `ThreePeopleDemo()` est executée depuis `Main()`

```csharp
public class FuncDemo
{
    private static Person person1 = new Person("Pierre", 27);
    private static Person person2 = new Person("Pauline", 39);
    private static Person person3 = new Person("Jacques", 41);

    static int SumAges(Person p1, Person p2, Person p3) => p1.Age + p2.Age + p3.Age;
    static int AverageAges(Person p1, Person p2, Person p3) => (p1.Age + p2.Age + p3.Age) / 3;

    public static void ThreePeopleDemo()
    {
        Console.WriteLine(person1);
        Console.WriteLine(person2);
        Console.WriteLine(person3);

        // delegate taking 3 people as params and returning an integer
        // delegate prenant 3 personnes en param et renvoi un entier
        // SumAges assigned to delegate
        // SumAges assigné au délégué
        Func<Person, Person, Person, int> myFunc = SumAges;

        System.Console.WriteLine(myFunc(person1, person2, person3));

        // AverageAges assigned to delegate
        // AverageAges assigné au délégué
        myFunc = AverageAges;

        System.Console.WriteLine(myFunc.Invoke(person1, person2, person3));

        // Anonymous declaration scope
        // Déclaration anonyme en bloc
        AskFunc((Person p1, Person p2, Person p3) =>
        {
            // return new List<Person>{p1,p2,p3}.Min();
            int temp = new List<int> { p1.Age, p2.Age, p3.Age }.Min();

            return temp;
        });

        Person mikoshi = new Person("Mikoshi", 38);
        Person liam = new Person("Liam", 12);
        Person renata = new Person("Renata", 23);

        // Anonymous declaration with added params to pass to the delegate
        // Déclaration anonyme avec paramètres additionels à passer au delegate
        AskFunc(mikoshi, liam, renata, (p1, p2, p3) =>
        {
            System.Console.WriteLine($"{p1.Name} name's length is {p1.Name.Count()}");
            System.Console.WriteLine($"{p2.Name} name's length is {p2.Name.Count()}");
            System.Console.WriteLine($"{p3.Name} name's length is {p3.Name.Count()}");

            int total = p1.Name.Count() + p2.Name.Count() + p3.Name.Count();
            System.Console.WriteLine($"Total {total}");

            return total;
        });
    }

    // Methods asking for a Func in their parameters
    // Méthodes demandant un Func dans les paramètres
    static void AskFunc(Func<Person, Person, Person, int> threePeopleFunc)
    {
        AskFunc(person1, person2, person3, threePeopleFunc);
    }

    static void AskFunc(Person person1, Person person2, Person person3, Func<Person, Person, Person, int> threePeopleFunc)
    {
        System.Console.WriteLine($"AskFunc => {threePeopleFunc(person1, person2, person3)}");
    }
}
```

> Output
> Sortie

```
[Name: Pierre, Age: 27]
[Name: Pauline, Age: 39]
[Name: Jacques, Age: 41]
107
35
AskFunc => 27
Mikoshi name's length is 7
Liam name's length is 4
Renata name's length is 6
Total 17
AskFunc => 17
```

## Java

> Class(e) `Person`

```java
public class Person {
	private String name;
	private int age;
	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
}
```

> Interface `ThreePeople`

```java
@FunctionalInterface
public interface ThreePeople {
	int doWithAges(Person p1, Person p2, Person p3);
}
```

> Misc. equivalent examples. `threePeopleDemo()` is ran from `main()`
> Divers exemples équivalents. `threePeopleDemo()` est exécutée depuis `main()`

I don't know how to reproduce something equivalent to the commented part in here.

Je ne voit pas comment faire l'équivalent à la partie commentée ici.

```java
public class FuncEquivalentDemo {
	private static Person person1 = new Person("Pierre", 27);
	private static Person person2 = new Person("Pauline", 39);
	private static Person person3 = new Person("Jacques", 41);

	static int sumAges(Person p1, Person p2, Person p3) {
		return p1.getAge() + p2.getAge() + p3.getAge();
	}

	static int averageAges(Person p1, Person p2, Person p3) {
		return (p1.getAge() + p2.getAge() + p3.getAge()) / 3;
	}

	public static void threePeopleDemo() {
		System.out.println(person1);
		System.out.println(person2);
		System.out.println(person3);

		// delegate prenant 3 personnes en param et renvoi un entier
		// SumAges assigné
		// ThreePeople myFunc = SumAges;
		
		// System.out.println(myFunc(person1, person2, person3));

		// AverageAges assigné
		// myFunc = AverageAges;

		// System.out.println(myFunc(person1, person2, person3));

		// Déclaration anonyme en bloc
		askFunc((Person p1, Person p2, Person p3) -> {

			return Collections.min(new ArrayList<Integer>() {

				private static final long serialVersionUID = -1886324149251243947L;

				{
					add(p1.getAge());
					add(p2.getAge());
					add(p3.getAge());
				}
			});
		});

		Person mikoshi = new Person("Mikoshi", 38);
		Person liam = new Person("Liam", 12);
		Person renata = new Person("Renata", 23);

		// Déclaration anonyme avec paramètres pesonnalisables
		// C'est surtout ça que j'ai envie de pouvoir faire

		askFunc(mikoshi, liam, renata, (p1, p2, p3) -> {
			System.out.println(p1.getName() + " names's length is " + p1.getName().length());
			System.out.println(p2.getName() + " names's length is " + p2.getName().length());
			System.out.println(p3.getName() + " names's length is " + p3.getName().length());

			int total = p1.getName().length() + p2.getName().length() + p3.getName().length();
			System.out.println(total);

			return total;
		});
	}

	static void askFunc(ThreePeople threePeople) {
		askFunc(person1, person2, person3, threePeople);
	}

	static void askFunc(Person person1, Person person2, Person person3, ThreePeople threePeople) {
		System.out.println("askFunc -> " + threePeople.doWithAges(person1, person2, person3));
	}
}
```

> Sortie

```
Person [name=Pierre, age=27]
Person [name=Pauline, age=39]
Person [name=Jacques, age=41]
askFunc -> 27
Mikoshi names's length is 7
Liam names's length is 4
Renata names's length is 6
17
askFunc -> 17
```